const express = require("express");
const mongoose = require("mongoose");

const usersAuth = require("./routes/api/usersAuth");
// const profile = require('./routes/api/usersAuth');
const formations = require("./routes/api/formations");

const app = express();

// DB config
const db = require("./config/keys").mongoURI;

// Connect to MongoDB
mongoose
  .connect(db)
  .then(() => console.log("MongoDB Connected"))
  .catch(err => console.log(err));

app.get("/", (req, res) => res.send("Pito"));

// Use Routes
app.use("/api/usersAuth", usersAuth);
app.use("/api/formations", formations);

const port = process.env.port || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
