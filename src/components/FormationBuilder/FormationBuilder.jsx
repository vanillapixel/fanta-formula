import React, { Component } from "react";
import classes from "./FormationBuilder.css";
import Drivers from "../Drivers/Drivers.jsx";

class formationBuilder extends Component {
  state = {
    drivers: [
      { id: 1, name: "Hamilton", value: 50, disabled: false },
      { id: 2, name: "Bottas", value: 38, disabled: false },
      { id: 3, name: "Vettel", value: 45, disabled: false },
      { id: 4, name: "LeClerc", value: 42, disabled: false },
      { id: 5, name: "Magnussen", value: 28, disabled: false },
      { id: 6, name: "Grosjeans", value: 22, disabled: false },
      { id: 7, name: "Sainz", value: 24, disabled: false },
      { id: 8, name: "Kyviat l'assassino", value: 12, disabled: false },
    ],
    formation: [],
    total: null,
  };

  removeDriverFromList(id) {
    const newDrivers = [...this.state.drivers]
    const newFormation = [...this.state.formation]
    const selectedDriver = newDrivers[id]
    newDrivers.splice(id, 1);
    this.setState({
      drivers: newDrivers
    })
    newFormation.push(selectedDriver)
    newFormation.sort((a, b) => a.id - b.id)
    this.setState({
      formation: newFormation
    })
    const totalArray = newFormation.map(a => a.value)
    const total = totalArray.reduce((partial_sum, a) => partial_sum + a)
    this.setState({
      total: total
    })
  }

  removeDriverFromFormation(id) {
    const newDrivers = [...this.state.drivers]
    const newFormation = [...this.state.formation]
    const selectedDriver = newFormation[id]
    newFormation.splice(id, 1);
    this.setState({
      formation: newFormation
    })
    newDrivers.push(selectedDriver)
    newDrivers.sort((a, b) => a.id - b.id)
    this.setState({
      drivers: newDrivers
    })
    const totalArray = newFormation.map(a => a.value)
    console.log(totalArray)
    const total = (totalArray.length > 0) ? totalArray.reduce((partial_sum, a) => partial_sum + a) : 0
    this.setState({
      total: total
    })
  }

  render() {
    return (
      <React.Fragment>
        <section className={classes.formationSection}>
          <Drivers
            clicked={this.removeDriverFromList.bind(this)}
            title="Lista dei driver"
            driversList={this.state.drivers}
            disabled={this.state.disabled}
          />
          <Drivers
            clicked={this.removeDriverFromFormation.bind(this)}
            title="Formazione"
            driversList={this.state.formation}
          />
          <h2 className={(this.state.total > 155) ? [classes.total, classes.totalExceeded].join(' ') : classes.total}>Totale: {this.state.total}{(this.state.total > 155) ? <h3>Hai speso un qualcosa come {this.state.total - 155} piotte de troppo. Datte na regolata zzì</h3> : null}</h2>

        </section>
      </React.Fragment>
    );
  }
}

export default formationBuilder;