import React from "react";
import classes from "./Drivers.css";


const Drivers = props => {
  const driversNames = props.driversList
  return (
    <React.Fragment>
      <div className={classes.container}>
        <h1 className={classes.driversListTitle}>{props.title}</h1>
        <ul className={classes.driversList}>
          {driversNames.map((driver, id) => {
            return (
              <div className={classes.driverContainer}>
                <li
                  className={(props.disabled) ? [classes.red, classes.driver].join(' ') : classes.driver}
                  onClick={() => props.clicked(id)}
                  key={driver.id}>{driver.name}
                </li>
                <span className={classes.pricetag}>{driver.value} mln</span>
              </div>
            )
          })}
        </ul>
      </div>
    </React.Fragment >
  );
};

export default Drivers;
