import React, { Component } from "react";
import "./App.css";
import FormationBuilder from "./components/FormationBuilder/FormationBuilder.jsx";

class App extends Component {
  state = {
    players: {
      Giorgio: {
        points: "",
        favPilot: "pito"
      },
      Riccardo: {
        points: "",
        favPilot: "serpe"
      },
      Andrea: {
        points: "",
        favPilot: "lemmo"
      }
    }
  };
  render() {
    return (
      <React.Fragment>
        <FormationBuilder />
      </React.Fragment>
    );
  }
}

export default App;
