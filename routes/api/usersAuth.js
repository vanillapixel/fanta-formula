const express = require("express");
const router = express.Router();

// @route GET api/usersAuth/test
// @desc Tests post route
// @access Public

router.get("/test", (req, res) => res.json({ message: "usersAuth works" }));

module.exports = router;
