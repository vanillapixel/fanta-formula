const express = require("express");
const router = express.Router();

// @route GET api/formations/test
// @desc Tests post route
// @access Public

router.get("/test", (req, res) => res.json({ message: "Formations works" }));

module.exports = router;
